---
title: Hello DevOps
author: Mario Sergio
authorURL: http://twitter.com/
authorFBID: 100002976521003
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

<!--truncate-->

## Agora sim!

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque elementum dignissim ultricies. Fusce rhoncus ipsum tempor eros aliquam consequat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus elementum massa eget nulla aliquet sagittis. Proin odio tortor, vulputate ut odio in, ultrices ultricies augue. Cras ornare ultrices lorem malesuada iaculis. Etiam sit amet libero tempor, pulvinar mauris sed, sollicitudin sapien.

```
BERTO.ppm                      MENU_FUNCOES.ppm
AJUDA.ppm                       MSG_ALCOOL.ppm
CADASTROCLUBE.ppm               NADA_PAGAR.ppm
CANCELAR_ITEM.ppm               OBRIGADO.ppm
CLUBE.ppm                       ORGANICOS.ppm
CODIGO_SEGURANCA.ppm            PERGUNTA_COOPERADO.ppm
COLOQUE_ITEM_NA_BALANCA.ppm     PERGUNTA_ESTACIONAMENTO.ppm
COLOQUE_ITEM_NA_SACOLA.ppm      PERGUNTA_NFP.ppm
CONFIRMA_COOPERADO.ppm          PERGUNTA_NFP_SIMNAO.ppm
CONSULTANDO_COOPERADO.ppm       PERGUNTA_SOCIO_TORCEDOR.ppm
CONSULTANDO_SOCIO_TORCEDOR.ppm  PGTO_ESTACIONAMENTO.ppm
CONSULTA.ppm                    PLAN_CREDITO.ppm
DOTZ_END_SALE.ppm               PLANOS.ppm
ENVIANDO_SAT.ppm                PRE_VENDA.ppm
ERRO_CODIGO_BARRAS.ppm          PROCESSANDO.ppm
ERRO_PESO.ppm                   RETIRE_PRODUTO_NAO_REGISTRADO.ppm
ERRO.ppm                        RETIRE_SACOLA.ppm
ESCOLHA_DESCONTO.ppm            RETORNE_ITEM_PARA_SACOLA.ppm
FECHADO.ppm                     SCREEN_dotz.xml
FECHADOZ.ppm                    SCREEN.xml
FRUTAS.ppm                      SCREEN.xml~
ID_CPF_CUPOM.ppm                SECAO.ppm
ID_CPF_DOTZ.ppm                 SUBTOTAL_dotz.ppm
IDENTIFICACAO_ERRO.ppm          SUBTOTAL_PASSE_CARTAO.ppm
ID_EXISTENTE.ppm                SUBTOTAL.ppm
INFORMA_COOPERADO.ppm           SUBTOTAL_SENHA_CARTAO.ppm
INFORMA_ESTACIONAMENTO.ppm      TEFADM.ppm
INFORMA_NFP.ppm                 tema.ppm
INFORMA_SOCIO_TORCEDOR.ppm      VENDA.ppm
LEGUMES.ppm                     VENDA_TECLADO.ppm
LIVRE.ppm                       VERDURAS.ppm

```